<?php

namespace App\EventListener;

use App\Exceptions\BaseException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Class ExceptionListener
 *
 * @package App\EventListener
 * @category Listener
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $response = new Response();
        $response->setStatusCode(500);
        $response->setContent($exception->getMessage());

        if ($exception instanceof BaseException) {
            $message = [
                'error' => $exception->getMessage()
            ];
            $response->setStatusCode($exception->getCode());
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($message));
        }

        // HttpExceptionInterface is a special type of exception that holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        }

        $event->setResponse($response);
    }
}