<?php

namespace App\Services;

use App\Entity\Group;
use App\Entity\User;
use App\Exceptions\Services\CreateGroupAlreadyExistsException;
use App\Exceptions\Services\CreateGroupUnknownErrorException;
use App\Exceptions\Services\DeleteGroupByIdNotEmptyException;
use App\Exceptions\Services\GetAllGroupsNotFoundException;
use App\Exceptions\Services\GroupNotFoundException;
use App\Exceptions\Services\UpdateGroupAlreadyExistsException;
use App\Exceptions\Services\UpdateGroupUnknownErrorException;
use App\Factories\GroupFactory;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class UsersService
 *
 * @package App\Services
 * @category Service
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class GroupsService
{
    private $entityManager;
    private $groupFactory;

    /**
     * GroupsService constructor.
     * Sets all dependency injected resources needed by the service
     *
     * @param EntityManagerInterface $entityManager Doctrine's entity manager
     * @param GroupFactory           $groupFactory  A factory to create groups
     */
    public function __construct (EntityManagerInterface $entityManager, GroupFactory $groupFactory){
        $this->entityManager = $entityManager;
        $this->groupFactory = $groupFactory;
    }

    /**
     * Gets all groups
     *
     * @return array An array of Group
     *
     * @throws GetAllGroupsNotFoundException No groups were found
     */
    public function getAllGroups(): array {
        $groups = $this->entityManager->getRepository(Group::class)->findAll();
        if (empty($groups)) {
            throw new GetAllGroupsNotFoundException();
        }

        return $groups;
    }

    /**
     * Gets a group by id
     *
     * @param int $id The group's id
     *
     * @return Group The persisted group
     *
     * @throws GroupNotFoundException No group was found with the specified id
     */
    public function getGroupById(int $id): Group
    {
        $group = $this->entityManager->getRepository(Group::class)->findOneBy(['id' => $id]);
        if(empty($group)){
            throw new GroupNotFoundException();
        }

        return $group;
    }

    /**
     * Creates and persists a group
     *
     * @param string $name The group's name
     *
     * @return Group The persisted group
     *
     * @uses GroupFactory::createGroup
     *
     * @throws CreateGroupAlreadyExistsException A group was found with the specified name
     * @throws CreateGroupUnknownErrorException  An unexpected error has occurred
     */
    public function createGroup(string $name): Group {
        $group = $this->groupFactory->createGroup($name);

        $this->entityManager->persist($group);

        try {
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new CreateGroupAlreadyExistsException();
        } catch (Exception $exception) {
            throw new CreateGroupUnknownErrorException();
        }

        return $group;
    }

    /**
     * Updates a group by id
     * Does not upsert the group!
     *
     * @param int    $id   The group's id
     * @param string $name The group's name
     *
     * @uses GroupsService::getGroupById
     *
     * @throws UpdateGroupAlreadyExistsException An group was found with the specified name
     * @throws UpdateGroupUnknownErrorException  An unexpected error has occurred
     */
    public function updateGroupById(int $id, string $name) {
        $group = $this->getGroupById($id);
        $group->setName($name);

        $this->entityManager->persist($group);
        try {
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new UpdateGroupAlreadyExistsException();
        } catch (Exception $exception) {
            throw new UpdateGroupUnknownErrorException();
        }
    }

    /**
     * Deletes a group
     * A group can only be deleted if no users are associated with it
     *
     * @param int $id The group's id
     *
     * @uses GroupsService::getGroupById
     *
     * @throws DeleteGroupByIdNotEmptyException The group has associated users
     */
    public function deleteGroupById(int $id): void {
        $group = $this->getGroupById($id);

        if (count($group->getUsers())) {
            throw new DeleteGroupByIdNotEmptyException();
        }

        $this->entityManager->remove($group);
        $this->entityManager->flush();
    }
}