<?php

namespace App\Services;

use App\Entity\User;
use App\Exceptions\Services\CreateUserAlreadyExistsException;
use App\Exceptions\Services\CreateUserUnknownErrorException;
use App\Exceptions\Services\GetAllUsersNotFoundException;
use App\Exceptions\Services\UpdateUserAlreadyExistsException;
use App\Exceptions\Services\UpdateUserUnknownErrorException;
use App\Exceptions\Services\UserNotFoundException;
use App\Factories\UserFactory;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class UsersService
 *
 * @package App\Services
 * @category Service
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class UsersService
{
    private $entityManager;
    private $userFactory;
    private $groupsService;

    /**
     * UsersService constructor.
     * Sets all dependency injected resources needed by the service
     *
     * @param EntityManagerInterface $entityManager Doctrine's entity manager
     * @param UserFactory            $userFactory   A factory to create users
     * @param GroupsService          $groupsService Groups service, to easily manage users_groups relation
     */
    public function __construct (EntityManagerInterface $entityManager, GroupsService $groupsService, UserFactory $userFactory){
        $this->entityManager = $entityManager;
        $this->userFactory = $userFactory;
        $this->groupsService = $groupsService;
    }

    /**
     * Gets all users
     *
     * @return array An array of User
     *
     * @throws GetAllUsersNotFoundException No users were found
     */
    public function getAllUsers(): array {
        $users = $this->entityManager->getRepository(User::class)->findAll();
        if (empty($users)) {
            throw new GetAllUsersNotFoundException();
        }

        return $users;
    }

    /**
     * Get an user by id
     *
     * @param int $id The user's id
     *
     * @return User The persisted user
     *
     * @throws UserNotFoundException No user was found with the specified id
     */
    public function getUserById(int $id): User
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]);
        if(empty($user)){
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * Creates and persists a user
     *
     * @param string $name The user's name
     *
     * @return User The persisted user
     *
     * @uses UserFactory::createUser
     *
     * @throws CreateUserAlreadyExistsException An user was found with the specified name
     * @throws CreateUserUnknownErrorException  An unexpected error has occurred
     */
    public function createUser(string $name): User {
        $user = $this->userFactory->createUser($name);

        $this->entityManager->persist($user);

        try {
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new CreateUserAlreadyExistsException();
        } catch (Exception $exception) {
            throw new CreateUserUnknownErrorException();
        }

        return $user;
    }

    /**
     * Updates a user by id
     * Does not upsert the group!
     *
     * @param int    $id   The user's id
     * @param string $name The user's name
     *
     * @uses UsersService::getUserById
     *
     * @throws UpdateUserAlreadyExistsException An user was found with the specified name
     * @throws UpdateUserUnknownErrorException  An unexpected error has occurred
     */
    public function updateUserById(int $id, string $name) {
        $user = $this->getUserById($id);
        $user->setName($name);

        $this->entityManager->persist($user);
        try {
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new UpdateUserAlreadyExistsException();
        } catch (Exception $exception) {
            throw new UpdateUserUnknownErrorException();
        }
    }

    /**
     * Deletes an user
     *
     * @param int $id The user's id
     *
     * @uses UsersService::getUserById
     */
    public function deleteUserById(int $id): void {
        $user = $this->getUserById($id);

        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    /**
     * Adds a user to a group
     * If a user is already associated with the specified group no action is performed but no error is thrown
     *
     * @param int $userId  The user's id
     * @param int $groupId The group's id
     *
     * @uses UsersService::getUserById
     * @uses GroupsService::getGroupById
     */
    public function addUserToGroup(int $userId, int $groupId): void {
        $user = $this->getUserById($userId);
        $group = $this->groupsService->getGroupById($groupId);

        $user->addGroup($group);

        $this->entityManager->flush();
    }

    /**
     * Removes a user from a group
     * If a user is not associated with the specified group no action is performed but no error is thrown
     *
     * @param int $userId  The user's id
     * @param int $groupId The group's id
     *
     * @uses UsersService::getUserById
     * @uses GroupsService::getGroupById
     */
    public function removeUserFromGroup(int $userId, int $groupId): void {
        $user = $this->getUserById($userId);
        $group = $this->groupsService->getGroupById($groupId);

        $user->removeGroup($group);

        $this->entityManager->flush();
    }
}
