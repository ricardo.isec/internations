<?php

namespace App\Entity;

use App\Exceptions\Entities\UserAlreadyAssociatedWithGroupException;
use App\Exceptions\Entities\UserNotAssociatedWithGroupException;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UsersRepository;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 * @ORM\Table(name="users")
 */
class User implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=126, unique=true)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
     * @ORM\JoinTable(name="users_groups")
     */
    private $groups;

    /**
     * Associate the user to a group.
     *
     * @param Group $group The group to associate to the user
     *
     * @throws UserAlreadyAssociatedWithGroupException The user is already associated with specified group
     */
    public function addGroup(Group $group): void
    {
        if ($this->groups->contains($group)) {
            throw new UserAlreadyAssociatedWithGroupException();
        }

        $this->groups->add($group);
    }

    /**
     * Dissociates the user from a group.
     *
     * @param Group $group The group to dissociate to the user
     *
     * @throws UserNotAssociatedWithGroupException The user is not associated with specified group
     */
    public function removeGroup(Group $group): void
    {
        if (!$this->groups->contains($group)) {
            throw new UserNotAssociatedWithGroupException();
        }

        $this->groups->removeElement($group);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGroups()
    {
        return $this->groups;
    }

    public function setGroups($groups): void
    {
        $this->groups = $groups;
    }

    /**
     * Exposes all class properties as an array
     *
     * @return array An array with all the properties of the class
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
