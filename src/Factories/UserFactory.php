<?php

namespace App\Factories;

use App\Entity\User;

/**
 * Class UserFactory
 *
 * @package App\Factories
 * @category Factory
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class UserFactory {

    /**
     * Creates users
     *
     * @param string $name The user's name
     *
     * @return User The created user
     */
    public function createUser(string $name): User {
        $user = new User();
        $user->setName($name);

        return $user;
    }
}