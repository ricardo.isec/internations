<?php

namespace App\Factories;

use App\Entity\Group;

/**
 * Class GroupFactory
 *
 * @package App\Factories
 * @category Factory
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class GroupFactory {

    /**
     * Creates groups
     *
     * @param string $name The group's name
     *
     * @return Group The created group
     */
    public function createGroup(string $name): Group {
        $group = new Group();
        $group->setName($name);

        return $group;
    }
}