<?php


namespace App\Controller;


use App\Entity\User;
use App\Repository\UsersRepository;
use App\Services\GroupsService;
use App\Services\UsersService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class GroupsController
 *
 * @package App\Controller
 * @category Controller
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class GroupsController extends BaseController
{
    private $groupsService;

    public function __construct(GroupsService $groupsService)
    {
        parent::__construct();
        $this->groupsService = $groupsService;
    }


    /**
     * Retrieves all groups
     *
     * @Rest\Get("/groups")
     *
     * @return Response
     */
    public function getAllGroups() {
        $groups = $this->groupsService->getAllGroups();

        $this->response->setContent(json_encode($groups, $this->jsonOptions));

        return $this->response;
    }

    /**
     * Gets a group by id
     *
     * @Rest\Get("/groups/{id}")
     *
     * @param int $id The groups's id
     *
     * @return Response
     */
    public function getGroupById(int $id){
        $group = $this->groupsService->getGroupById($id);

        $this->response->setContent(json_encode($group, $this->jsonOptions));

        return $this->response;
    }

    /**
     * Creates a group
     * Returns the created group if creation is successful
     *
     * @Rest\Post("/groups")
     *
     * @param Request $request The HTTP Request
     *
     * @return Response
     */
    public function createGroup(Request $request) {
        $groupName = $request->get('name');
        $group = $this->groupsService->createGroup($groupName);

        $this->response->setStatusCode(201);
        $this->response->setContent(json_encode($group, $this->jsonOptions));

        return $this->response;
    }

    /**
     * Updates a group
     *
     * @Rest\Put("/groups/{id}")
     *
     * @param int     $id      The group's id
     * @param Request $request The HTTP Request
     *
     * @return Response
     */
    public function updateGroupById(int $id, Request $request) {
        $groupName = $request->get('name');
        $this->groupsService->updateGroupById($id, $groupName);

        $this->response->setStatusCode(204);

        return $this->response;
    }


    /**
     * Deletes a group
     *
     * @Rest\Delete("/groups/{id}")
     *
     * @param int $id The group's id
     *
     * @return Response
     */
    public function deleteGroupById($id){
        $this->groupsService->deleteGroupById($id);

        $this->response->setStatusCode(204);

        return $this->response;
    }
}