<?php


namespace App\Controller;


//use App\Entity\User;
//use App\Repository\UsersRepository;
use App\Services\UsersService;
//use FOS\RestBundle\Controller\AbstractFOSRestController;
//use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class UsersController
 *
 * @package App\Controller
 * @category Controller
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class UsersController extends BaseController
{
    private $usersService;

    public function __construct(UsersService $usersService)
    {
        parent::__construct();
        $this->usersService = $usersService;
    }


    /**
     * Retrieves all users
     * @Rest\Get("/users")
     *
     * @return Response
     */
    public function getAllUsers() {
        $users = $this->usersService->getAllUsers();

        $this->response->setContent(json_encode($users, $this->jsonOptions));

        return $this->response;
    }


    /**
     * Gets a user by id
     *
     * @Rest\Get("/users/{id}")
     *
     * @param int $id The user's id
     *
     * @return Response
     */
    public function getUserById(int $id){
        $user = $this->usersService->getUserById($id);

        $this->response->setContent(json_encode($user, $this->jsonOptions));

        return $this->response;
    }

    /**
     * Creates a user
     * Returns the created user if creation is successful
     *
     * @Rest\Post("/users")
     *
     * @param Request $request The HTTP Request
     *
     * @return Response
     */
    public function createUser(Request $request) {
        $userName = $request->get('name');
        $user = $this->usersService->createUser($userName);

        $this->response->setStatusCode(201);
        $this->response->setContent(json_encode($user, $this->jsonOptions));

        return $this->response;
    }

    /**
     * Update a user
     *
     * @Rest\Put("/users/{id}")
     *
     * @param int     $id      The user's id
     * @param Request $request The HTTP Request
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateUserById(int $id, Request $request) {
        $userName = $request->get('name');
        $this->usersService->updateUserById($id, $userName);

        $this->response->setStatusCode(204);

        return $this->response;
    }

    /**
     * Deletes a user
     *
     * @Rest\Delete("/users/{id}")
     *
     * @param int $id The user's id
     *
     * @return Response
     */
    public function deleteUserById($id){
        $this->usersService->deleteUserById($id);

        $this->response->setStatusCode(204);

        return $this->response;
    }

    /**
     * Adds a user to a group
     *
     * @Rest\Post("/users/{userId}/groups/{groupId}")
     *
     * @param int $userId  The user's id
     * @param int $groupId The group's id
     *
     * @return Response
     */
    public function addUserToGroup(int $userId, int $groupId) {
        $this->usersService->addUserToGroup($userId, $groupId);

        $this->response->setStatusCode(204);

        return $this->response;
    }

    /**
     * Removes a user from a group
     *
     * @Rest\Delete("/users/{userId}/groups/{groupId}")
     *
     * @param int $userId  The user's id
     * @param int $groupId The group's id
     *
     * @return Response
     */
    public function removeUserFromGroup(int $userId, int $groupId) {
        $this->usersService->removeUserFromGroup($userId, $groupId);

        $this->response->setStatusCode(204);

        return $this->response;
    }
}