<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractFOSRestController
{
    protected $response;
    protected $jsonOptions;

    public function __construct()
    {
        $this->response = new Response();
        $this->response->setStatusCode(200);
        $this->response->headers->set('Content-Type', 'application/json');
        $this->jsonOptions = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES |
            //  JSON_THROW_ON_ERROR /* PHP 7.3 only */ |
            JSON_PARTIAL_OUTPUT_ON_ERROR;
    }
}