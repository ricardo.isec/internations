<?php

namespace App\Exceptions\Services;

use App\Exceptions\BaseException;

/**
 * Class GetAllGroupsNotFoundException
 *
 * @package App\Exceptions\Services
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class GetAllGroupsNotFoundException extends BaseException
{
    protected const CODE = 404;
    protected const MESSAGE = 'No groups were found';

    public function __construct() {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}