<?php

namespace App\Exceptions\Services;

use App\Exceptions\BaseException;

/**
 * Class DeleteGroupByIdNotEmptyException
 *
 * @package App\Exceptions\Services
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class DeleteGroupByIdNotEmptyException extends BaseException
{
    protected const CODE = 409;
    protected const MESSAGE = 'Group is not empty';

    public function __construct() {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}