<?php

namespace App\Exceptions\Services;

use App\Exceptions\BaseException;

/**
 * Class UpdateUserAlreadyExistsException
 *
 * @package App\Exceptions\Services
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class UpdateUserAlreadyExistsException extends BaseException
{
    protected const CODE = 409;
    protected const MESSAGE = 'User already exists';

    public function __construct() {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}