<?php

namespace App\Exceptions\Services;

use App\Exceptions\BaseException;

/**
 * Class GetAllUsersNotFoundException
 *
 * @package App\Exceptions\Services
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class GetAllUsersNotFoundException extends BaseException
{
    protected const CODE = 404;
    protected const MESSAGE = 'No users were found';

    public function __construct() {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}