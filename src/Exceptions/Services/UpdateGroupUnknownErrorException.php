<?php

namespace App\Exceptions\Services;

use App\Exceptions\BaseException;

/**
 * Class UpdateGroupUnknownErrorException
 *
 * @package App\Exceptions\Services
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class UpdateGroupUnknownErrorException extends BaseException
{
    protected const CODE = 500;
    protected const MESSAGE = 'An unknown error has occurred while updating the group';

    public function __construct() {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}