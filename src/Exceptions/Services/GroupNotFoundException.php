<?php

namespace App\Exceptions\Services;

use App\Exceptions\BaseException;

/**
 * Class GroupNotFoundException
 *
 * @package App\Exceptions\Services
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class GroupNotFoundException extends BaseException
{
    protected const CODE = 404;
    protected const MESSAGE = 'group not found';

    public function __construct() {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}