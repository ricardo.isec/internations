<?php

namespace App\Exceptions\Services;

use App\Exceptions\BaseException;

/**
 * Class UserNotAssociatedWithGroupException
 *
 * @package App\Exceptions\Services
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class CreateGroupUnknownErrorException extends BaseException
{
    protected const CODE = 500;
    protected const MESSAGE = 'An unknown error has occurred while creating the group';

    public function __construct() {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}