<?php

namespace App\Exceptions;

use Exception;
use Throwable;

/**
 * Exception base
 * Base class for all application exceptions.
 *
 * @package  App\Exceptions
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
abstract class BaseException extends Exception
{
    protected const CODE_DEFAULT = 500;
    protected const MESSAGE_DEFAULT = "Sorry, the server was unable to complete your request... It's not you, it's us!";

    /**
     * Class constructor.
     * This constructor expands PHP's default \Exception constructor with default codes and messages.
     *
     * @param string    $message  The intended message, a default will be used if none provided
     * @param int       $code     The intended code, a default will be used if none provided
     * @param Throwable $previous The previous extension called (not used)
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) Parameters were included to comply with PHP documentation
     */
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct();
        $this->message = empty($message) ? static::MESSAGE_DEFAULT : $message;
        $this->code = $code == 0 ? static::CODE_DEFAULT : $code;
    }
}
