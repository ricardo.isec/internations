<?php

namespace App\Exceptions\Entities;

use App\Exceptions\BaseException;

/**
 * Class UserAlreadyAssociatedWithGroupException
 *
 * @package App\Exceptions\Entities
 * @category Exception
 *
 * @author    Ricardo Cabete <cabete@gmail.com>
 * @copyright 2019 Ricardo Cabete
 */
class UserAlreadyAssociatedWithGroupException extends BaseException
{
    protected const CODE = 409;
    protected const MESSAGE = 'The user is already associated with that group';

    public function __construct() {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}